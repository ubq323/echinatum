use rand::{thread_rng, Rng};
use crate::coords;

pub const CHUNK_SIZE: usize = 32;

// obviously this will be greatly expanded in future
#[derive(Debug,Copy,Clone,Eq,PartialEq)]
pub enum Tile {
    Blank,
    Orange
}

pub struct Chunk {
    pub pos: coords::ChunkPos,
    pub tiles: Option<[Tile; CHUNK_SIZE*CHUNK_SIZE]>
}

impl Chunk {
    fn index(offset: coords::Hex) -> usize {
        // panics if offset is out of bounds
        // so don't pass it an offset that is out of bounds
        let chunk_size = CHUNK_SIZE as i32;
        if 0 <= offset.q() && offset.q() < chunk_size && 0 <= offset.r() && offset.r() < chunk_size {
            let q = offset.q() as usize;
            let r = offset.r() as usize;
            q + r*CHUNK_SIZE
        } else {
            panic!("hex offset out of chunk bounds")
        }
    }

    pub fn tile_at_offset(&self, offset: coords::Hex) -> Option<Tile> {
        // if index is bad return none
        match self.tiles {
            Some(tiles) => {
                let idx = Chunk::index(offset);
                Some(tiles[idx])
            },
            None => None
        }
    }

    pub fn set_tile_at_offset(&mut self, offset: coords::Hex, value: Tile) {
        // this should probably report errors back eventually
        if let Some(tiles) = self.tiles.as_mut() {
            let idx = Chunk::index(offset);
            tiles[idx] = value;
        }
    }

    pub fn tmp_new_random() -> Chunk {
        let mut tiles = [Tile::Blank; CHUNK_SIZE*CHUNK_SIZE];
        let mut rng = thread_rng();
        for i in 0..CHUNK_SIZE*CHUNK_SIZE {
            let n = rng.gen_range(0,2);
            tiles[i] = match n {
                0 => Tile::Blank,
                _ => Tile::Orange,
            };
        }

        Chunk { pos: coords::ChunkPos::new(0,0), tiles: Some(tiles) }
    }
}
