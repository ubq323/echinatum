use std::ops::{Add, Sub, Neg, Mul, Div};
use serde::{Serialize, Deserialize};

// a general 2d position.
// is generic, unlike hex
#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub struct Pos {
    x: f64,
    y: f64,
}

// should probably have been called vec2d or something
// oh well
impl Pos {
    pub fn new(x: f64, y: f64) -> Pos {
        Pos { x, y }
    }

    pub fn x(self) -> f64 { self.x }
    pub fn y(self) -> f64 { self.y }

    pub fn to_hex(self) -> super::Hex {
        let q = ((3.0f64.sqrt() / 3.0)*self.x()) - self.y()*(1.0/3.0);
        let r = (2.0/3.0)*self.y();
        super::Hex::round3(q,r,-q-r)
    }

    pub fn len_squared(self) -> f64 {
        self.x.powi(2) + self.y.powi(2)
    }
    pub fn len(self) -> f64 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }

    pub fn norm(self) -> Pos {
        if self.len_squared() == 0. {
            self
        } else {
            self / self.len()
        }
    }
}

impl std::fmt::Display for Pos {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "({:.2},{:.2})", self.x, self.y)
    }
}


impl Add for Pos {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}
impl Sub for Pos {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}
impl Mul<f64> for Pos {
    type Output = Self;
    fn mul(self, other: f64) -> Self {
        Self {
            x: self.x * other,
            y: self.y * other,
        }
    }
}
impl Div<f64> for Pos {
    type Output = Self;
    fn div(self, other: f64) -> Self {
        Self {
            x: self.x / other,
            y: self.y / other,
        }
    }
}
impl Neg for Pos {
    type Output = Self;
    fn neg(self) -> Self {
        Self {
            x: -self.x,
            y: -self.y,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Pos;
    use super::super::Hex;
    #[test]
    fn accessing_i() {
        let p = Pos::new(7.0,9.0);
        assert_eq!(p.x(), 7.0);
        assert_eq!(p.y(), 9.0);
    }

    #[test]
    fn adding() {
        let p1 = Pos::new(12.0, -6.0);
        let p2 = Pos::new(3.0, -3.0);
        let sum = p1 + p2;

        assert_eq!(sum.x(), 15.0);
        assert_eq!(sum.y(), -9.0);
    }
    #[test]
    fn subtracting() {
        let p1 = Pos::new(-2.0,2.0);
        let p2 = Pos::new(7.0, -6.0);
        let r = p1 - p2;
        assert_eq!(r.x(), -9.0);
        assert_eq!(r.y(), 8.0);
    }
    #[test]
    fn negating() {
        let p = Pos::new(3.0,-5.0);
        let r = -p;
        assert_eq!(r.x(), -3.0);
        assert_eq!(r.y(), 5.0);
    }
    #[test]
    fn multiplying() {
        let p = Pos::new(3.0, -8.0);
        let r = p * 3.0;
        assert_eq!(r.x(), 9.0);
        assert_eq!(r.y(), -24.0);
    }
    #[test]
    fn dividing() {
        let p = Pos::new(5.0, -6.0);
        let r = p / 2.0;
        assert_eq!(r.x(), 2.5);
        assert_eq!(r.y(), -3.0);
    }
    #[test]
    fn length() {
        let p = Pos::new(1.0, 1.0);
        let l = p.len();
        assert_eq!(l, 2f64.sqrt());
    }
    #[test]
    fn length_squared() {
        let p = Pos::new(1.0, 1.0);
        let l = p.len_squared();
        assert_eq!(l, 2.0);
    }
    #[test]
    fn norm() {
        let p = Pos::new(1.0, 1.0);
        let r = p.norm();
        // floating point rounding errors
        assert!((r - Pos::new(2f64.sqrt()/2., 2f64.sqrt()/2.)).len().abs() < 0.000001);
    }
    #[test]
    fn equality() {
        let p1 = Pos::new(7.0,8.0);
        let p2 = Pos::new(7.0,8.0);
        assert_eq!(p1,p2);
    }
    #[test]
    fn inequality() {
        let p1 = Pos::new(9.8, 2.6);
        let p2 = Pos::new(1.3, 9.0);
        assert_ne!(p1,p2);
    }
    #[test]
    fn to_hex() {
        let p = Pos::new(15.0,8.1);
        let h = p.to_hex();
        assert_eq!(h, Hex::new3(6,5,-11));
        assert_eq!(p, Pos::new(15.0,8.1));
    }
}
