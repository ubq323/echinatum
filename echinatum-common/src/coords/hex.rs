use std::ops::{Add, Sub};

use super::{Pos, ChunkPos};
use crate::chunk::CHUNK_SIZE;
use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub struct Hex {
    q: i32,
    r: i32,
    s: i32,
}

impl Hex {
    pub fn new2(q: i32, r: i32) -> Hex {
        Hex { q, r, s: -q-r, }
    }

    pub fn new3(q: i32, r: i32, s: i32) -> Hex {
        assert_eq!(q+r+s, 0, "hex doesn't meet invariant");
        Hex { q, r, s }
    }

    pub fn q(self) -> i32 { self.q }
    pub fn r(self) -> i32 { self.r }
    pub fn s(self) -> i32 { self.s }

    pub fn round3(fq: f64, fr: f64, fs: f64) -> Hex {
        let mut rq = fq.round();
        let mut rr = fr.round();
        let mut rs = fs.round();

        let dq = (fq-rq).abs();
        let dr = (fr-rr).abs();
        let ds = (fs-rs).abs();

        if dq > dr && dq > ds {
            rq = -rr-rs;
        } else if dr > ds {
            rr = -rq-rs;
        } else {
            rs = -rq-rr;
        }
        Hex::new3(rq as i32, rr as i32, rs as i32)
    }

    pub fn to_pos(self) -> Pos {
        let qf = self.q() as f64;
        let rf = self.r() as f64;
        let x = (qf*3.0f64.sqrt()) + (rf*(3.0f64.sqrt()/2.0));
        let y = (3.0/2.0)*rf;
        return Pos::new(x,y);
    }

    pub fn chunk(self) -> ChunkPos {
        ChunkPos::new(
            ((self.q() as f64) / (CHUNK_SIZE as f64)).floor() as i32,
            ((self.r() as f64) / (CHUNK_SIZE as f64)).floor() as i32
        )
    }

    pub fn chunk_and_offset(self) -> (ChunkPos, Hex) {
        let chunk = self.chunk();
        let offset = self - chunk.min();
        (chunk, offset)
    }

    pub fn offset(self) -> Hex {
        self.chunk_and_offset().1
    }


}

impl Add for Hex {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        Self {
            q: self.q + other.q,
            r: self.r + other.r,
            s: self.s + other.s,
        }
    }
}
impl Sub for Hex {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        Self {
            q: self.q - other.q,
            r: self.r - other.r,
            s: self.s - other.s,
        }
    }
}

impl std::fmt::Display for Hex {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f,"{{{}, {}}}",self.q,self.r)
    }
}
#[cfg(test)]
mod tests {
    use super::Hex;
    use crate::coords::ChunkPos;
    use crate::chunk::CHUNK_SIZE;
    #[allow(non_upper_case_globals)] // it isn't a global, silly compiler
    const chunk_size: i32 = CHUNK_SIZE as i32;
    #[test]
    fn accessing() {
        let h = Hex::new3(4, 6, -10);
        assert_eq!(h.q(), 4);
        assert_eq!(h.r(), 6);
        assert_eq!(h.s(), -10);
    }
    #[test]
    fn accesing2() {
        let h = Hex::new2(7, -3);
        assert_eq!(h.q(), 7);
        assert_eq!(h.r(), -3);
        assert_eq!(h.s(), -4);
    }
    #[test]
    fn adding() {
        let h1 = Hex::new3(1, 2, -3);
        let h2 = Hex::new3(7, -6, -1);
        let sum = h1 + h2;
        assert_eq!(sum.q(), 8);
        assert_eq!(sum.r(), -4);
        assert_eq!(sum.s(), -4);
    }
    #[test]
    fn subtracting() {
        let h1 = Hex::new3(1,2,-3);
        let h2 = Hex::new3(7, -6, -1);
        let r = h1 - h2;
        assert_eq!(r.q(), -6);
        assert_eq!(r.r(), 8);
        assert_eq!(r.s(), -2);
    }
    #[test]
    #[should_panic]
    fn equals_zero() {
        let _ = Hex::new3(1,1,1);
    }
    #[test]
    fn equality() {
        let h1 = Hex::new3(1,2,-3);
        let h2 = Hex::new3(1,2,-3);
        assert_eq!(h1,h2);
    }
    #[test]
    fn inequality() {
        let h1 = Hex::new3(1,2,-3);
        let h2 = Hex::new3(-1,-2,3);
        assert_ne!(h1,h2);
    }
    #[test]
    fn rounding1() {
        // these aren't real coordinates
        // it still does test the algorithm though
        // but this could probably be made better
        assert_eq!(Hex::round3(0.55,0.6,0.6), Hex::new3(-2,1,1));
        assert_eq!(Hex::round3(-3.4, 6.0, 7.9), Hex::new3(-14, 6, 8));
    }
    #[test]
    fn to_pos() {
        let h = Hex::new2(1,1);
        let p = h.to_pos();
        assert_eq!(p.x(),(3.0/2.0)*3f64.sqrt());
        assert_eq!(p.y(),1.5);
    }
    #[test]
    fn to_pos_and_back() {
        for q in -20..=20 {
            for r in -20..=20 {
                let h1 = Hex::new2(q,r);
                let h2 = h1.to_pos().to_hex();
                assert_eq!(h1,h2);
            }
        }
    }
    #[test]
    fn chunk() {
        let tl = Hex::new2(-1,-1);
        let tr = Hex::new2(0,-1);
        let bl = Hex::new2(-1,0);
        let br = Hex::new2(0,0);

        let h2 = Hex::new2(chunk_size,chunk_size);

        assert_eq!(tl.chunk(), ChunkPos::new(-1,-1));
        assert_eq!(tr.chunk(), ChunkPos::new(0,-1));
        assert_eq!(bl.chunk(), ChunkPos::new(-1,0));
        assert_eq!(br.chunk(), ChunkPos::new(0,0));

        assert_eq!(h2.chunk(), ChunkPos::new(1,1));
    }
    #[test]
    fn offset() {
        assert_eq!(Hex::new2(0,0).offset(), Hex::new2(0,0));
        assert_eq!(Hex::new2(chunk_size,chunk_size).offset(), Hex::new2(0,0));
        assert_eq!(Hex::new2(-1,-1).offset(), Hex::new2(chunk_size-1,chunk_size-1));
    }


}
