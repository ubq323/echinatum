use crate::chunk::CHUNK_SIZE;

use serde::{Serialize, Deserialize};

use super::Hex;
// x and y correlate to the q and r directions respectively
#[derive(Debug, Eq, PartialEq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct ChunkPos {
    x: i32,
    y: i32,
}
// not implementing adding or subtracting because i don't think it will be
// necessary. if it ever turns out to be, i will add it.
impl ChunkPos {
    pub fn new(x: i32, y: i32) -> ChunkPos {
        ChunkPos { x, y }
    }

    pub fn min(self) -> Hex {
        Hex::new2(self.x*CHUNK_SIZE as i32,self.y*CHUNK_SIZE as i32)
    }
    pub fn max(self) -> Hex {
        Hex::new2(
            (self.x+1)*(CHUNK_SIZE as i32)-1,
            (self.y+1)*(CHUNK_SIZE as i32)-1
        )
    }

    pub fn x(self) -> i32 { self.x }
    pub fn y(self) -> i32 { self.y }

}

#[cfg(test)]
mod tests {
    use super::ChunkPos;
    use super::CHUNK_SIZE;
    use crate::coords::Hex;
    #[test]
    fn accessing() {
        let c = ChunkPos::new(5,-2);
        assert_eq!(c.x(), 5);
        assert_eq!(c.y(), -2);
    }
    #[test]
    fn min_max_1() {
        let c = ChunkPos::new(0,0);
        let max = c.max();
        let min = c.min();
        assert_eq!(min, Hex::new2(0,0));
        assert_eq!(max, Hex::new2(CHUNK_SIZE as i32-1, CHUNK_SIZE as i32-1));
    }
    #[test]
    fn adjacency() {
        for x in -20..=20 {
            for y in -20..=20 {
                let c = ChunkPos::new(x,y);
                let cmin = c.min();
                let cmax = c.max();

                let tl = ChunkPos::new(x-1,y-1);
                let tr = ChunkPos::new(x+1,y-1);
                let bl = ChunkPos::new(x-1,y+1);
                let br = ChunkPos::new(x+1,y+1);

                let ml = ChunkPos::new(x-1,y  );
                let mr = ChunkPos::new(x+1,y  );
                let tm = ChunkPos::new(x  ,y-1);
                let bm = ChunkPos::new(x  ,y+1);

                assert_eq!(tl.max().q(),cmin.q()-1);
                assert_eq!(ml.max().q(),cmin.q()-1);
                assert_eq!(bl.max().q(),cmin.q()-1);

                assert_eq!(tr.min().q(),cmax.q()+1);
                assert_eq!(mr.min().q(),cmax.q()+1);
                assert_eq!(br.min().q(),cmax.q()+1);

                assert_eq!(tm.min().q(),cmin.q());
                assert_eq!(tm.max().q(),cmax.q());
                assert_eq!(bm.min().q(),cmin.q());
                assert_eq!(bm.max().q(),cmax.q());


                assert_eq!(tl.max().r(),cmin.r()-1);
                assert_eq!(tm.max().r(),cmin.r()-1);
                assert_eq!(tr.max().r(),cmin.r()-1);

                assert_eq!(bl.min().r(),cmax.r()+1);
                assert_eq!(bm.min().r(),cmax.r()+1);
                assert_eq!(br.min().r(),cmax.r()+1);

                assert_eq!(ml.min().r(),cmin.r());
                assert_eq!(ml.max().r(),cmax.r());
                assert_eq!(mr.min().r(),cmin.r());
                assert_eq!(mr.max().r(),cmax.r());
            }
        }
    }
}
