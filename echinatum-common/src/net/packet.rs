// packet types and so on

use serde::{Serialize, Deserialize};
use crate::coords;

#[derive(Serialize, Deserialize, Clone)]
pub enum ServerBoundPacket {
    ChatMessage(String),
    PlayerLocation(coords::Pos),
}

#[derive(Serialize, Deserialize, Clone)]
pub enum ClientBoundPacket {
    // these usizes should be replaced with some kind of player identifier, whether that's
    // some kind of connection id, entitiy id, or just a username string
    ChatMessage(usize, String),
    PlayerLocation(usize,coords::Pos),
}
