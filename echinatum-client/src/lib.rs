use std::cell::RefCell;
use std::rc::Rc;

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;


use echinatum_common::{coords, chunk};
use echinatum_common::net::packet::{ServerBoundPacket,ClientBoundPacket};

mod drawing;
mod input;
mod networking;

const SPEED: f64 = 0.5;

#[wasm_bindgen(module = "/js/chatinterface.js")]
extern "C" {
    fn display_chat_message(s: String);
}

// When the `wee_alloc` feature is enabled, this uses `wee_alloc` as the global
// allocator.
//
// If you don't want to use `wee_alloc`, you can safely delete this.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen(start)]
pub fn main_js() -> Result<(), JsValue> {
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();

    let mut chunk = chunk::Chunk::tmp_new_random();

    let window = web_sys::window().expect("couldn't get window");
    let document = window.document().expect("couldn't get document");
    let canvas = document.get_element_by_id("canvas").unwrap().dyn_into::<web_sys::HtmlCanvasElement>().unwrap();

    let mut camera = drawing::Camera::new(canvas.clone());

    let keys = input::Keys::create(&document).expect("couldn't make keyboard controller");
    let mouse = input::Mouse::create(&canvas).expect("couldn't make mouse controller");

    //let (websocket, incoming) = networking::create();
    let connection = networking::Connection::create();

    {
        let mut prevtime: f64 = 0.0;
        let f = Rc::new(RefCell::new(None));
        let g = f.clone();
        let keys = keys.clone();
        *g.borrow_mut() = Some(Closure::wrap(Box::new(move |timestamp: f64| {
            let deltatime = (timestamp-prevtime)/1000.0;
            if deltatime >= (1.0/40.0) {
                prevtime = timestamp;
                document.get_element_by_id("fps").unwrap()
                    .set_inner_html(&format!("{:.2}",(1.0/deltatime)));

                let mousepos = coords::Pos::new(mouse.x.get() as f64, mouse.y.get() as f64);
                let mouseworldpos = camera.screen_to_world(mousepos);
                let mousehexpos = mouseworldpos.to_hex();
                document.get_element_by_id("mousestatus").unwrap()
                    .set_inner_html(&format!("{} {}",mouse,mousehexpos));
                document.get_element_by_id("position").unwrap()
                    .set_inner_html(&format!("@{} z:{:.2}",camera.pos,camera.zoom));

                let mut dx = 0.;
                let mut dy = 0.;
                if keys.up.get() {
                    dy -= 1.;
                }
                if keys.down.get() {
                    dy += 1.;
                }

                if keys.left.get() {
                    dx -= 1.;
                }
                if keys.right.get() {
                    dx += 1.;
                }

                if keys.zoom_in.get() {
                    camera.zoom *= 1.1;

                    // this should be probably automatic beacuse it will
                    // be the desired behaviour for most keys
                    keys.zoom_in.set(false);
                }
                if keys.zoom_out.get() {
                    camera.zoom /= 1.1;
                    keys.zoom_out.set(false);
                }

                if let Some(s) = keys.chat_message.replace(None) {
                    let packet = ServerBoundPacket::ChatMessage(s.clone());
                    connection.send(&packet).unwrap();
                }

                if let Ok(packet) = connection.try_recv() {
                    if let ClientBoundPacket::ChatMessage(id,s) = packet {
                        display_chat_message(format!("[{}] {}",id,s));
                    }
                }

                if mousehexpos.chunk() == coords::ChunkPos::new(0,0) {
                    if mouse.left.get() {
                        chunk.set_tile_at_offset(mousehexpos, chunk::Tile::Orange);
                    }
                    if mouse.right.get() {
                        chunk.set_tile_at_offset(mousehexpos, chunk::Tile::Blank);
                    }
                }

                let movement = coords::Pos::new(dx,dy).norm() * SPEED;
                if movement.len_squared() != 0. {
                    camera.pos = camera.pos + movement;
                    connection.send(&ServerBoundPacket::PlayerLocation(camera.pos)).unwrap();
                }
                onframe(&chunk,&camera);
            }


            request_animation_frame(f.borrow().as_ref().unwrap());
        }) as Box<dyn FnMut(_)>));
        request_animation_frame(g.borrow().as_ref().unwrap());
    }

    Ok(())
}

// helper fn
fn request_animation_frame(f: &Closure<dyn FnMut(f64)>) {
    web_sys::window().unwrap()
            .request_animation_frame(f.as_ref().unchecked_ref())
            .expect("should register requestanimationframe");
}

fn onframe(chunk: &chunk::Chunk, camera: &drawing::Camera) {
    camera.clear();

    for q in 0..chunk::CHUNK_SIZE as i32 {
        for r in 0..chunk::CHUNK_SIZE as i32 {
            let h = coords::Hex::new2(q,r);
            if let Some(tile) = chunk.tile_at_offset(h) {
                if tile == chunk::Tile::Orange {
                    let p = h.to_pos();
                    camera.drawhex(p);
                }
            }
        }
    }

    camera.drawplayer(camera.pos,true);
    camera.drawplayer(coords::Pos::new(40.,40.),false);
}
