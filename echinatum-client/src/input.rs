use std::cell::{RefCell,Cell};
use std::rc::Rc;

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

#[derive(Debug)]
pub struct Keys {
    current_focus: Cell<Focus>,

    pub up: Cell<bool>,
    pub down: Cell<bool>,
    pub left: Cell<bool>,
    pub right: Cell<bool>,

    pub zoom_in: Cell<bool>,
    pub zoom_out: Cell<bool>,

    pub chat_message: RefCell<Option<String>>,

}

// handles keyboard input
// and also chat input because it's easier that way

#[derive(Copy,Clone,Debug,PartialEq,Eq)]
enum Focus {
    Game,
    Chat,
}

impl Keys {
    fn new() -> Keys {
        Keys {
            current_focus: Cell::new(Focus::Game),

            up: Cell::new(false),
            down: Cell::new(false),
            left: Cell::new(false),
            right: Cell::new(false),

            zoom_in: Cell::new(false),
            zoom_out: Cell::new(false),

            chat_message: RefCell::new(None),
        }
    }

    pub fn create(document: &web_sys::Document) -> Result<Rc<Keys>, JsValue> {
        let chatbox = document.get_element_by_id("chatbox").unwrap().dyn_into::<web_sys::HtmlInputElement>()?;

        let keys = Rc::new(Keys::new());
        {
            let keys = keys.clone();
            let chatbox = chatbox.clone();
            let closure = Closure::wrap(Box::new(move |event: web_sys::KeyboardEvent| {
                match keys.current_focus.get() {
                    Focus::Game => {
                        if let Some(c) = keys.named(&event.code()) {
                            c.set(true);
                        } else if event.code() == "Enter" {
                            // XXX don't make enter another keybind elsewhere
                            chatbox.focus().unwrap();
                            keys.set_focus(Focus::Chat);
                        }
                    },
                    Focus::Chat => {
                        if event.code() == "Enter" {
                            chatbox.blur().unwrap();
                            *keys.chat_message.borrow_mut() = Some(chatbox.value());
                            chatbox.set_value("");
                            keys.set_focus(Focus::Game);
                        }
                    }
                } 
            }) as Box<dyn FnMut(_)>);
            document.add_event_listener_with_callback("keydown", closure.as_ref().unchecked_ref())?;
            closure.forget();
        }
        {
            let keys = keys.clone();
            let closure = Closure::wrap(Box::new(move |event: web_sys::KeyboardEvent| {
                if keys.current_focus.get() == Focus::Game {
                    if let Some(c) = keys.named(&event.code()) {
                        c.set(false);
                    }
                }
            }) as Box<dyn FnMut(_)>);
            document.add_event_listener_with_callback("keyup", closure.as_ref().unchecked_ref())?;
            closure.forget();
        }

        // might as well do this here
        // this is slightly messy but it works
        {
            // on chatbox focus
            let keys = keys.clone();
            let chatbox = chatbox.clone();
            let closure = Closure::wrap(Box::new(move |_: web_sys::FocusEvent| {
                keys.set_focus(Focus::Chat);
            }) as Box<dyn FnMut(_)>);
            chatbox.add_event_listener_with_callback("focus", closure.as_ref().unchecked_ref())?;
            closure.forget();
        }
        {
            // on chatbox unfocus
            let keys = keys.clone();
            let chatbox = chatbox.clone();
            let closure = Closure::wrap(Box::new(move |_: web_sys::FocusEvent| {
                keys.set_focus(Focus::Game);
            }) as Box<dyn FnMut(_)>);
            chatbox.add_event_listener_with_callback("blur", closure.as_ref().unchecked_ref())?;
            closure.forget();
        }

        Ok(keys)
    }

    fn set_focus(&self, val: Focus) {
        self.current_focus.set(val);

        if val == Focus::Chat {
            self.up.set(false);
            self.down.set(false);
            self.left.set(false);
            self.right.set(false);

            self.zoom_in.set(false);
            self.zoom_out.set(false);
        }
    }

    fn named(&self, name: &str) -> Option<&Cell<bool>> {
        match name {
            "KeyW" => Some(&self.up),
            "KeyS" => Some(&self.down),
            "KeyA" => Some(&self.left),
            "KeyD" => Some(&self.right),

            "Minus" => Some(&self.zoom_out),
            "Equal" => Some(&self.zoom_in),
            _ => None,

        }
    }
}

#[derive(Debug)]
pub struct Mouse {
    pub left: Cell<bool>,
    pub right: Cell<bool>,
    pub middle: Cell<bool>,
    // x and y will store screen position
    // turning that into game coords will be done elsewhere
    pub x: Cell<i32>,
    pub y: Cell<i32>,
}

impl Mouse {
    fn new() -> Mouse {
        Mouse {
            left: Cell::new(false),
            right: Cell::new(false),
            middle: Cell::new(false),
            x: Cell::new(0),
            y: Cell::new(0),
        }
    }

    fn button_named(&self, name: i16) -> Option<&Cell<bool>> {
        match name {
            0 => Some(&self.left),
            1 => Some(&self.middle),
            2 => Some(&self.right),
            _ => None,
        }
    }

    pub fn create(canvas: &web_sys::Element) -> Result<Rc<Mouse>, JsValue> {
        let mouse = Rc::new(Mouse::new());
        {
            let mouse = mouse.clone();
            let closure = Closure::wrap(Box::new(move |event: web_sys::MouseEvent| {
                if let Some(button) = mouse.button_named(event.button()) {
                    button.set(true);
                }
                //web_sys::console::log_1(&format!("mouse down, {:?}", mouse).into());
            }) as Box<dyn FnMut(_)>);
            canvas.add_event_listener_with_callback("mousedown", closure.as_ref().unchecked_ref())?;
            closure.forget();
        }
        {
            let mouse = mouse.clone();
            let closure = Closure::wrap(Box::new(move |event: web_sys::MouseEvent| {
                if let Some(button) = mouse.button_named(event.button()) {
                    button.set(false);
                }
                //web_sys::console::log_1(&format!("mouse up, {:?}", mouse).into());
            }) as Box<dyn FnMut(_)>);
            canvas.add_event_listener_with_callback("mouseup", closure.as_ref().unchecked_ref())?;
            closure.forget();
        }
        {
            let mouse = mouse.clone();
            let closure = Closure::wrap(Box::new(move |event: web_sys::MouseEvent| {
                mouse.x.set(event.client_x());
                mouse.y.set(event.client_y());
            }) as Box<dyn FnMut(_)>);
            canvas.add_event_listener_with_callback("mousemove", closure.as_ref().unchecked_ref())?;
            closure.forget();
        }
        Ok(mouse)
    }
}

impl std::fmt::Display for Mouse {
    // not permanent
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let l = if self.left.get() { 'L' } else { '-' };
        let r = if self.right.get() { 'R' } else { '-' };
        let m = if self.middle.get() { 'M' } else { '-' };
        write!(f, "{}{}{} ({},{})", l,m,r, self.x.get(), self.y.get())
    }
}
