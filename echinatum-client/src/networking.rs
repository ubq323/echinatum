// todo: some sort of queue for incoming unprocessed messages,
// wrapped in a rwlock or something

// or a mpsc or something

// just a test for now though

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::WebSocket;

use std::sync::mpsc;

use echinatum_common::net::packet::{ServerBoundPacket, ClientBoundPacket};

// using this to give us a nicer api in the rest of the app
// and to abstract away the actual details of the api,
// in case we ever end up changing that for whatever reason.

pub struct Connection {
    ws: WebSocket,
    incoming: mpsc::Receiver<ClientBoundPacket>,
}

impl Connection {

    pub fn create() -> Connection {
        let ws = WebSocket::new("ws://localhost:6969/ws").unwrap();
        ws.set_binary_type(web_sys::BinaryType::Arraybuffer);

        let (tx, rx) = mpsc::channel();


        /*{   // this doesn't do anything atm
            let ws = ws.clone();
            let ws2 = ws.clone();
            let callback = Closure::wrap(Box::new(move|_| {
                ws.send_with_u8_array(&payload).unwrap();
            }) as Box<dyn FnMut(JsValue)>);
            ws2.set_onopen(Some(callback.as_ref().unchecked_ref()));
            callback.forget();
        }*/
        {

            // using a mpsc to send incoming messages to the rest of the application
            // because they give us a synchronous nonblocking recieving interface
            // rather than the callback-based async one we get with the websocket
            // by itself.
            let ws2 = ws.clone();
            let callback = Closure::wrap(Box::new(move|event: web_sys::MessageEvent| {
                if let Ok(buf) = event.data().dyn_into::<js_sys::ArrayBuffer>() {
                    let array = js_sys::Uint8Array::new(&buf);
                    match serde_cbor::from_slice(&array.to_vec()) {
                        Ok(packet) => {
                            if let Err(e) = tx.send(packet) {
                                web_sys::console::log_1(&format!("sending error: {:?}",e).into());
                            };
                        },
                        Err(e) => {
                            web_sys::console::log_1(&format!("deserialisation error {:?} {:?}",e,array).into());
                        }
                    };
                }
            }) as Box<dyn FnMut(_)>);
            ws2.set_onmessage(Some(callback.as_ref().unchecked_ref()));
            callback.forget();
        }
        {
            let callback = Closure::wrap(Box::new(move|_: web_sys::ErrorEvent| {
                web_sys::window().unwrap().alert_with_message("websocket error. this probably means the server is broken. sorry.").unwrap();
            }) as Box<dyn FnMut(_)>);
            ws.set_onerror(Some(callback.as_ref().unchecked_ref()));
            callback.forget();
        }

        Connection{ws, incoming: rx}


    }
    

    pub fn send(&self, packet: &ServerBoundPacket) -> Result<(), String> {
        let payload = serde_cbor::to_vec(&packet).map_err(|_| "couldn't serialize".to_string())?;
        self.ws.send_with_u8_array(&payload).map_err(|_| "couldn't send down websocket".to_string())?;

        Ok(())
    }

    pub fn try_recv(&self) -> Result<ClientBoundPacket, mpsc::TryRecvError> {
        self.incoming.try_recv()
    }
}
