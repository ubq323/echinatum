// camera and drawing functions
use echinatum_common::coords::Pos;
mod js {
    use wasm_bindgen::prelude::*;
    #[wasm_bindgen(module="/js/drawing.js")]
    extern "C" {
        // cx and cy are postitons on the canvas of the centre of the hex,
        // size is the centre-to-vertex distance in canvas units
        pub fn drawhex(cx: f64, cy: f64, size: f64);
        // clear the canvas
        pub fn clear();
        // draw a player at a given position
        pub fn drawplayer(x: f64, y: f64, size: f64, currentplayer: bool);
    }
}


// camera is a class that stores the current zoom and posititon
// which can also convert between screen coords and world coords
// by looking at the current width and height of the canvas

// let camera.zoom = canvas units per world unit

// let camera.pos (w) = centre of screen (c)

// world to screen:
// (w-camera)*zoom + (width/2, height/2)

// screen to world:
// (s-(width/2, height/2))/zoom + camera


pub struct Camera {
    canvas: web_sys::HtmlCanvasElement,
    pub zoom: f64,
    pub pos: Pos,
}

impl Camera {
    pub fn new(canvas: web_sys::HtmlCanvasElement) -> Camera {
        // todo have these as params maybe
        let zoom = 20.;
        let pos = Pos::new(0.0,0.0);


        Camera { canvas, zoom, pos }
    }

    pub fn new_with(canvas: web_sys::HtmlCanvasElement, zoom: f64, pos: Pos) -> Camera {
        Camera { canvas, zoom, pos }
    }

    fn screen_offset(&self) -> Pos {
        Pos::new(
            self.canvas.width() as f64 / 2.,
            self.canvas.height() as f64 / 2.
        )
    }

    pub fn world_to_screen(&self, world: Pos) -> Pos {
        ((world - self.pos) * self.zoom) + self.screen_offset()
    }
    pub fn screen_to_world(&self, screen: Pos) -> Pos {
        ((screen - self.screen_offset())/self.zoom) + self.pos
    }

    pub fn clear(&self) {
        js::clear();
    }
    pub fn drawhex(&self, wpos: Pos) {
        let spos = self.world_to_screen(wpos);
        js::drawhex(spos.x(), spos.y(), self.zoom);
    }
    pub fn drawplayer(&self, wpos: Pos, localplayer: bool) {
        let spos = self.world_to_screen(wpos);
        js::drawplayer(spos.x(), spos.y(), self.zoom*2., localplayer);
    }
    
}
