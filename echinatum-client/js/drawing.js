let canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d");

function resize() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    //ctx.imageSmoothingEnabled = false;
}
resize();
window.addEventListener("resize",resize);

let corners = [];
for (let corner = 0; corner < 6; corner++) {
    let angle = (2*Math.PI/12)+corner * (2*Math.PI)/6;
    let x = (Math.cos(angle));
    let y = (Math.sin(angle));
    corners.push([x,y]);
}


export function clear() {
    ctx.clearRect(0,0,canvas.width,canvas.height);
}

export function drawhex(cx, cy, size) {
    ctx.fillStyle = "#ff7900";
    ctx.strokeStyle = "black";
    ctx.lineWidth = size * 0.05;
    ctx.beginPath();
    ctx.moveTo(cx+corners[0][0]*size, cy+corners[0][1]*size);
    for (let c = 1; c < 6; c++) {
        ctx.lineTo(cx+corners[c][0]*size, cy+corners[c][1]*size);
    }
    ctx.closePath();
    ctx.fill();
    ctx.stroke();
}

export function drawplayer(x,y,size,localplayer) {
    if (localplayer) {
        ctx.fillStyle = "#e79e00";
    } else {
        ctx.fillStyle = "lightsteelblue"
    }
    ctx.fillRect(x-(size/2),y-(size/2),size,size);
}