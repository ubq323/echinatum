// for displaying chat messages
// doing in js because easier

let messages = document.getElementById("chatmessages");

export function display_chat_message(s) {
    let content = document.createTextNode(s);
    let element = document.createElement("p");
    element.classList.add("chatmessage");
    element.appendChild(content);

    messages.append(element);

    setTimeout(()=>{
        element.style.opacity=0;
        setTimeout(()=>{
            element.remove();
        },500);
    }, 5000);
}