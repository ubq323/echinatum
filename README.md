# echinatum development is now using fossil instead of git. go to <https://f.gh0.pw/echinatum/home> for the actual developed version.

# echinatum
online multiplayer game with technology and machines and things

this is still in a very early stage of development. indeed, there is not anything
you could call a game at the moment.

`echinatum` is a temporary/working title, but i can't think of anything else yet
so it will stay for now.

`echinatum-server` is the serverside component, `echinatum-client` is the client side
(compiled to webassembly to run in a web browser), and `echinatum-common` is code shared
by both parts.

currently everything is version `0.1.0`. once there is some actual content/gameplay
i will start using the version for something actually meaningful.

## running for yourself
why? there isn't anything here...

you will need rust, npm and wasm-pack.

### to compile/run the server
`cargo run` in the `echinatum-server` crate (or `cargo-build` or whatever).

### to compile/run the client
`npm run build` in the `echinatum-client` crate. then serve from the `dist`
subfolder.
