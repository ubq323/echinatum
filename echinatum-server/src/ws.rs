// will contain all the websocket related things

use crate::{Connections, NEXT_USER_ID};

use warp::{Filter, filters::BoxedFilter, Reply, Rejection};
use warp::ws::{self, WebSocket};
use tokio::sync::mpsc;
use futures::{StreamExt,SinkExt};
use ring::hmac;

use std::sync::atomic::Ordering;

use echinatum_common::net::packet::{ServerBoundPacket,ClientBoundPacket};

async fn user_connect(ws: WebSocket, incoming_tx: mpsc::UnboundedSender<(ServerBoundPacket,usize)>, connections: Connections) {
    // we need to be able to handle sending and recieving in separate tasks (which is the easiest way to structure this)
    let (mut ws_tx, mut ws_rx) = ws.split();

    // generate the connection id here. todo: rename this to connection id 
    let this_user_id = NEXT_USER_ID.fetch_add(1, Ordering::Relaxed);
    info!("new user {} connectied", this_user_id);

    // forward outgoing messages to websocket
    // create mpsc for outgoing messages
    let (outgoing_tx, mut outgoing_rx) = mpsc::unbounded_channel::<ClientBoundPacket>();
    // in a separate task, send out any outgoing messages sent to us by the main loop
    tokio::task::spawn(async move {
        while let Some(packet) = outgoing_rx.next().await {
            let payload = match serde_cbor::to_vec(&packet) {
                Ok(v) => v,
                Err(e) => { 
                    error!("packet serialisation error: {}",e);
                    continue;
                }
            };
            let ws_msg = ws::Message::binary(payload);
            if let Err(e) = ws_tx.send(ws_msg).await {
                error!("websocket send error (uid={}): {}",this_user_id,e);
            }
        }
    });

    // insert the mpsc for outgoing messages into the connections hashmap
    connections.write().await.insert(this_user_id, outgoing_tx);

    // forward incoming messages to the main incoming messages mpsc
    // todo: disconnect on error, instead of ignoring
    while let Some(res) = ws_rx.next().await {
        let msg = match res {
            Ok(msg) => msg,
            Err(e) => {
                error!("websocket recv error (uid={}): {}",this_user_id,e);
                break;
            }
        };

        let packet: ServerBoundPacket = match serde_cbor::from_slice(msg.as_bytes()) {
            Ok(packet) => packet,
            Err(e) => {
                error!("packet deserialisation error: {}",e);
                continue;
            }
        };

        if let Err(e) = incoming_tx.send((packet,this_user_id)) {
            error!("mpsc send error (uid={}): {}",this_user_id,e);
            break;
        };
    }

    // once we get to this point either there has been an error or the client has disconnected
    // remove the connection from the hashmap
    connections.write().await.remove(&this_user_id);
    info!("user {} disconnected", this_user_id);

}

pub fn ws_route(incoming_tx: mpsc::UnboundedSender<(ServerBoundPacket,usize)>, connections: Connections, key: &hmac::Key) -> BoxedFilter<(impl Reply,)> {
    // incoming_tx is the mpsc we will send incoming messages down
    // connections is the shared hashmap of connection id to mpsc of outgoing messages.
    let key = key.clone();
    warp::path("ws")
        .and(warp::ws())
        .and(warp::any().map(move || incoming_tx.clone()))
        .and(warp::any().map(move || connections.clone()))
        .and(warp::cookie("auth"))
        .and(warp::any().map(move || key.clone()))
        .and_then(handle_ws).boxed()
}

async fn handle_ws(ws: warp::ws::Ws, incoming_tx: mpsc::UnboundedSender<(ServerBoundPacket, usize)>, connections: Connections, auth: String, key: hmac::Key) -> Result<impl Reply, Rejection> {
    if crate::web::valid_auth(auth, &key) {
        Ok(ws.on_upgrade(move |websocket| user_connect(websocket, incoming_tx, connections)))
    } else {
        Err(warp::reject::reject())
    }
}