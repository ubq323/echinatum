// this will contain the web interface
// auth stuff will probably be in a different file eventually

fn is_valid_login(username: String, password: String) -> bool {
    // a very terrible way of doing things
    (username=="user1" && password=="pass1")
    ||(username=="user2" && password=="pass2")
}

use warp::{Filter, filters::BoxedFilter, Reply};
use serde::{Serialize,Deserialize};
use warp::http::Response;
use ring::hmac;


#[derive(Deserialize)]
struct LoginRequest {
    username: String,
    password: String,
}

pub fn create_key() -> hmac::Key {
    // generating a new key every time we are started
    // because it doesn't really matter
    let rng = ring::rand::SystemRandom::new();
    hmac::Key::generate(hmac::HMAC_SHA256, &rng).expect("couldn't generate hmac key")
}

pub fn routes(key: &hmac::Key) -> BoxedFilter<(impl Reply,)> {
    let key = key.clone();
    let key2 = key.clone();

    let index_route = warp::any().and(warp::path::end()).map(|| {
        warp::reply::html("<h1>echinatum</h1><p>this will ultimately be the main page of the application</p>")
    });
    
    let see_route = warp::path("h").and(warp::filters::cookie::optional("auth"))
    .map(move |cookie: Option<String>| -> Result<Response<warp::hyper::Body>,warp::http::Error> {
        let (disp, clear) = match cookie {
            Some(s) => {
                if let Ok(json) = base64::decode(s) {
                    if let Ok(payload) = serde_json::from_slice::<AuthPayload>(&json) {
                        if hmac::verify(&key2, payload.username.as_bytes(), payload.hmac.as_ref()).is_ok() {
                            (format!("i found {} with a valid hmac",payload.username), false)
                        } else {
                            (format!("i found {} but with an invalid hmac",payload.username), true)
                        }
                        
                    } else {
                        ("deserialisation error".to_string(), true)
                    }
                } else {
                    ("base64 decode error".to_string(), true)
                }
            },
            None => ("couldn't find anything".to_string(), false)
        };
        let mut builder = Response::builder();
        if clear {
            builder = builder.header("Set-Cookie","auth=h; Expires=Wed, 21 Oct 2015 01:00:00 GMT"); // some random date in the past
        }
        builder.body(disp.into())
    });

    let login_form_route = warp::get()
        .and(warp::path("login"))
        .map(|| {
            warp::reply::html("<h1>echinatum</h1><form method=post action=/login><input name=username><br><input type=password name=password><input type=submit value=go></form>")
        });
    let login_response_route = warp::post()
        .and(warp::path("login"))
        .and(warp::body::form())
        .map(move |l| do_login(l, &key));
    
    see_route.or(login_form_route).or(login_response_route).or(index_route).boxed()
}

#[derive(Serialize,Deserialize)]
struct AuthPayload {
    username: String,
    hmac: Vec<u8>,
}

fn do_login(l: LoginRequest, key: &hmac::Key) -> Result<Response<warp::hyper::Body>,warp::http::Error> {
    let builder = Response::builder();
    if is_valid_login(l.username.clone(), l.password) {
        // compute hmac
        // for now this is just username so it'll last forever so that's not a great way to do it
        let payload = l.username.as_bytes();
        let hash = hmac::sign(&key, payload);
        // this means there can't be any |s in any usernames but that's probably a reasonable restriction
        // possibly make this use json eventually instead
        let pl = AuthPayload{username: l.username.clone(), hmac: hash.as_ref().into()};
        let cval = serde_json::to_string(&pl).expect("cookie serialization error");
        builder.header("Set-Cookie",format!("auth={}",base64::encode(cval)))
            .body(format!("yes. {}",l.username).into())
    } else {
        builder.body("no, go away".into())
    }
    
}

pub fn valid_auth(content: String, key: &hmac::Key) -> bool {
    if let Ok(json) = base64::decode(content) {
        if let Ok(payload) = serde_json::from_slice::<AuthPayload>(&json) {
            if hmac::verify(&key, payload.username.as_bytes(), payload.hmac.as_ref()).is_ok() {
                return true;
            }
        }
    }
    false
}