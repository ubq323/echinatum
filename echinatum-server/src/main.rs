#[macro_use] extern crate log;

use std::collections::HashMap;
use std::sync::{
    atomic::AtomicUsize,
    Arc,
};

use tokio::sync::{mpsc, RwLock};
use warp::Filter;

mod web;
mod ws;
mod gameloop;

use echinatum_common::net::packet::ClientBoundPacket;

type Connections = Arc<RwLock<HashMap<usize, mpsc::UnboundedSender<ClientBoundPacket>>>>;
static NEXT_USER_ID: AtomicUsize = AtomicUsize::new(1);

/*
so time to reörganise this

the main loop of the game itself will be
(in no particular order)
respond to all pending ingame user input
perform one tick of the simulation
send any information about the world to logged in players
handle any new connections
handle any logging outs. 

that will all be done within a single task. otherwise we'd have all race conditions and things
and i don't think there'd be a performance benefit to doing that either.

so then the other parts of the program are going to be the websocket interface
which we already have, it multiplexes all incoming messages into a single mpsc which is what we want. 
it will need to send the id of the user ofc but we can work that out later. 
well it already does but it would be good if we have an actual way of getting username etc.

the other important thing is authentication, and generally the web interface surrounding 
the application itself. which will be done with extra warp routes probably in another file.
so the question is linking all that together neatly
and additionally linking the main loop part to the game simulation itself which will
presumably be the most complex part of the application, and will presumably live in
a specific place.
*/


#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    // setup secret key used for authentication
    // done here because centrally
    let key = web::create_key();

    // connections is the mapping of connection id (currently usize) to mpsc for outgoing messages.
    // each incoming message is also handed to the main loop with the corresponding usize id. 
    let connections = Connections::default();

    // all incoming messages are multiplexed onto this by the tasks listening on each ws connection, so that the 
    // main loop can get all incoming messages in one place easily.
    let (incoming_tx, incoming_rx) = mpsc::unbounded_channel();

    // the warp route for handling incoming ws connections, which also handles setting up the tasks to listen and 
    // send on each connection, and talking to the main loop here. these also handle tidying up when a client 
    // disconnects.
    let ws_route = ws::ws_route(incoming_tx, connections.clone(), &key);
    // authentication, landing page, etc. will serve the actual client eventually too
    let index_route = web::routes(&key);

    // get warp to serve in a separate task so we can run the main loop here. 
    let routes = ws_route.or(index_route);
    tokio::task::spawn(warp::serve(routes).run(([127,0,0,1], 6969))); // todo: put the address/port in a config file or something

    gameloop::gameloop(incoming_rx,connections).await;
}