use tokio::sync::mpsc;
use futures::StreamExt;

use crate::Connections;
use echinatum_common::net::packet::{ClientBoundPacket, ServerBoundPacket};

/*
so, currently this just processes every single message as soon as it arrives
obviously what we really want is for it to have a regular "tick" process as well, which 
will run the actual simulation, at a known speed.
so there are a couple of ways to do that. one is to have it such that, each n amount 
of time:
    - process all packets that have not been processed yet
    - compute one tick of the simulation
    - send whatever information out to all connected clients
ie, everything is in one single loop happening every tick. 
the alternative is to have two loops. one of them is, each n amount of time, compute one tick of
the simulation; and the other continually processes all packets and responds to them. that is, we have
a shared mutable state which both loops access continually. i feel like the first will be slightly 
easier to implement but that isn't really the deciding factor. normally shared mutable state
would be a bad thing but this is rust so we're probably ok wrt that. i guess performance should be 
the deciding factor, so i should probably test both at some point. for now i'll go with the first option i guess.
*/

/* 
other todo: have a proper system for mananging logged in users and connections. atm all we have is
a hashmap of "connection id" to "outgoing websocket", which isn't ideal. one way to solve this 
might be to have a separate hashmap of connection id to logged in user information thing. 
another way to do this might be to have a single hashmap of (something?) to user information thing, 
and have that user information thing contain the outgoing websocket.
things to consider here are how to keep all data structures agreeing with each other, as well as
(eg) how easy it is to kick a player. 
*/

pub async fn gameloop(
        mut incoming_rx: mpsc::UnboundedReceiver<(ServerBoundPacket, usize)>,
        connections: Connections,
    ) {
    while let Some((packet,sender_id)) = incoming_rx.next().await {
        let conns = connections.read().await;
    
        match packet {
            ServerBoundPacket::ChatMessage(content) => {
                info!("chat from {} => {}",sender_id, content);
    
                let response = ClientBoundPacket::ChatMessage(sender_id, content);

                for (uid, tx) in conns.iter() {
                    if let Err(e) = tx.send(response.clone()) {
                        error!("error sending message to uid {}: {}",uid,e);
                    }
                }
            },
            ServerBoundPacket::PlayerLocation(pos) => {
                info!("playerlocation from {} {}",sender_id,pos);

                let response = ClientBoundPacket::PlayerLocation(sender_id,pos);

                for (uid, tx) in conns.iter() {
                    if *uid != sender_id {
                        if let Err(e) = tx.send(response.clone()) {
                            error!("error sending message to uid {}: {}",uid,e);
                        }
                    }
                }
            }
        }
    }
}

